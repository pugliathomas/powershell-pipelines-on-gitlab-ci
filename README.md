# Slicing and Dicing with PowerShell in GitLab CI

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/gitlab-ci-yml-powershell-tips-tricks-and-hacks/powershell-pipelines-on-gitlab-ci)

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2020-04-14

* **GitLab Version Released On**: 12.10

* **GitLab Edition Required**: 

  * For overall solution: [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

- **Tested On**: 
  - GitLab Docker-Executor Runner (GitLab.com Shared Runner).
  - Windows Shell Runner (GitLab.com Shared Runner).

* **References and Featured In**:
  * Slicing and Dicing with PowerShell on GitLab CI: https://youtu.be/UZvtAYwruFc

## Demonstrates These Design Requirements, Desirements and AntiPatterns

- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Use PowerShell on a Windows Shell Runner (GitLab.com Shared Runner).
- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Use PowerShell Core / 7 (runtime installed) on a Windows Shell Runner (GitLab.com Shared Runner).
- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Use PowerShell Core / 7 on the prebuilt Microsoft .NET Core SDK Linux container. (Linux Containers are cheaper per minute if your code is compatible)
- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Use PowerShell Core / 7 (runtime installed) on an Amazon Linux container. (Linux Containers are cheaper per minute if your code is compatible)
- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Using all of the above in the same pipeline.
- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Passing artifacts between all of the above.

## Using PowerShell Under GitLab CI
* GitLab CI Variables are propagated to PowerShell as **both** environment variables and when PowerShell is the *default* shell for the runner also to regular PowerShell variables.
* Windows Shared runners are shell runners, so they do not have an "image:" tag associated.
* On GitLab.com runners run as the custom "gitlab-runner" account.
* Self-hosted runners can be setup as the system account or a named account: https://docs.gitlab.com/runner/install/windows.html#installation.
* PowerShell will be the default shell on Windows, unless you take specific actions to change it to CMD.
* The example shows a mix of Windows Shell runner and Linux Docker runner - runner switching is accomplished using tags.  Linux Docker is the default on GitLab.com, so these jobs have no tags.  To mix runners like this using self-hosted private runners, the necessary runners would be created with tags indicating what types of jobs they can take and then a mixed mode job liket his example will be possible.

### PowerShell on Windows
* The first two jobs show powershell running on Windows.  In this case it is the default shell.

### Using PowerShell Core
* **IMPORTANT** Since PowerShell core is the non-default shell, it only contains the GitLab CI variables as Environment Variables, whether running on Windows or Linux.
* PowerShell Core cannot currently be configured as the default shell, however, the ability to do so is under development: https://gitlab.com/gitlab-org/gitlab-runner/-/issues/13134

### Using PowerShell Core on Linux
* Just as using Linux machines on most clouds is cheaper than windows, the same is true of running your powershell code on linux
* An added benefit of running it on a Docker runner is being able to select the container you wish to use.

### Best Practices
* Given that the need to use a non-default shell on GitLab Runner results in only seeing GitLab CI Variables as environment variables, for maximum code reusability, it is advisable to code references to CI variables to the environment variable counter part.  (e.g. $env:CI_PROJECT_NAME)

### Cross References and Documentation

* Follow Gitlab CI PowerShell Issues on GitLab by using this issue search string: [https://gitlab.com/gitlab-org/gitlab-runner/-/issues?scope=all&utf8=✓&state=opened&label_name[]=Category%3ARunner&label_name[]=executor%3A%3Ashell&search=Powershell](https://gitlab.com/gitlab-org/gitlab-runner/-/issues?scope=all&utf8=✓&state=opened&label_name[]=Category%3ARunner&label_name[]=executor%3A%3Ashell&search=Powershell)
